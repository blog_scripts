all:
	dune build

release:
	dune build --profile=release

serve:
	cd _build/default && python3 -m http.server 8000

clean:
	dune clean

deps:
	opam install . --deps-only
