type t

(** Return the point id. Each id is unique *)
val id 
  : t -> int

val empty : t

val (+): t -> Gg.v2 -> t

val get_coord : t -> Gg.v2

val get_stamp : t -> float

val create: angle:float -> width:float -> stamp:float -> x:float -> y:float -> t

(** Return a copy of the point at given posistion 

    This is a true copy, the id will be the same for the two points
    TODO : Should this be renamed set_position ?

*)
val copy : t -> Gg.v2 -> t

val set_angle : t -> float -> t

val get_angle : t -> float

val set_width: t -> float -> t

val get_width: t -> float

val get_coord'
  : t -> Gg.v2

(** [mix f point p0 p1] create a new point at the position point, with the
    characteristics from p0 and p1 *)
val mix
  : float -> Gg.v2 -> t -> t -> t

