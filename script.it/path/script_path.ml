(** Common  module for ensuring that the function is evaluated only once *)

module Point = Point
module Repr = Repr
module Path_Builder = Builder.Make(Point)
module Fixed = Fixed.Make(Point)

