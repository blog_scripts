let internal_id = ref 0

type t =
  { p: Gg.v2
  ; size : float
  ; angle: float
  ; stamp : float
  ; id : int
  }

let empty =
  { p = Gg.V2.of_tuple (0., 0.)
  ; size = 0.
  ; angle = 0.
  ; stamp = 0.
  ; id = 0
  }

let create ~angle ~width ~stamp ~x ~y =

  incr internal_id;
  { p = Gg.V2.v x y
  ; size = width
  ; angle = Gg.Float.rad_of_deg (180. -. angle )
  ; stamp
  ; id = !internal_id
  }

let copy point p =
  { point with
    p
  }

let set_angle p angle =
  { p with angle = Gg.Float.rad_of_deg (180. -. angle) }

let get_angle { angle; _} = 180. -. (Gg.Float.deg_of_rad angle)

let set_width p size =
  { p with size }

let get_width { size; _} = size

let (+) p1 p2 =
  { p1 with p = Gg.V2.(+) p1.p p2 }

let get_coord { p; _ } = p

let get_stamp { stamp; _} = stamp

let get_coord'
  : t -> Gg.v2
  = fun t ->
    let open Gg.V2 in
    let trans = of_polar @@ v t.size t.angle in
    t.p + trans

let mix
  : float -> Gg.v2 -> t -> t -> t
  = fun f point p0 p1 ->
    let angle0 = p0.angle
    and angle1 = p1.angle
    and width0 = get_width p0
    and width1 = get_width p1
    and stamp0 = get_stamp p0
    and stamp1 = get_stamp p1 in
    let angle = angle0 +. f *. ( angle1 -. angle0 ) in
    let width = width0 +. f *. ( width1 -. width0 ) in
    let stamp = stamp0 +. f *. ( stamp1 -. stamp0 ) in
    { p = point
    ; size = width
    ; angle
    ; stamp
    ; id = !internal_id
    }

let id { id; _} = id
