(** Signature for points *)
module type P = sig
  type t

  val empty : t

  val get_coord : t -> Gg.v2

  (** Copy a point and all thoses properties to the given location *)
  val copy : t -> Gg.v2 -> t

end

module Make(Point:P) : sig

  type t

  (** Create an empty path *)
  val empty: t

  val add_point
    : Point.t -> t -> t

  (** Replace the last alement in the path by the one given in parameter *)
  val replace_last
    : Point.t -> t -> t

  (** Retrieve the last element, if any *)
  val peek 
    : t -> Point.t option

  (** Retrieve the last element, if any *)
  val peek2
    : t -> (Point.t * Point.t) option

  (** Represent the path *)
  val repr
    : t -> (module Repr.M with type point = Point.t and type t = 's) -> 's -> 's

  val map 
    : t -> (Point.t -> Point.t) -> t

end
