module type M = sig

  type point

  type t

  (* Start a new path. *)
  val start
    : point -> t -> t

  val line_to
    : point -> point -> t -> t

  val quadratic_to
    : (point * Gg.v2 * Gg.v2 * point) -> t -> t

  val stop
    : t -> t
end
