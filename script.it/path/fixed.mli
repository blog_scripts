(** Signature for points *)
module type P = sig
  type t

  val get_coord : t -> Gg.v2

  val id : t -> int

  val copy : t -> Gg.v2 -> t

end

module Make(Point:P) : sig

  module type BUILDER = sig
    type t

    val repr
      : t -> (module Repr.M with type point = Point.t and type t = 's) -> 's -> 's

  end

  type t 

  (** Create a path from a builder *)
  val to_fixed
    : (module BUILDER with type t = 'a) -> 'a -> t

  (** Represent the path *)
  val repr
    : t -> (module Repr.M with type point = Point.t and type t = 's) -> 's -> 's

  (** Structure to represent all the required information for evaluating the
      distance between a point and a path *)
  type approx =
    { distance : float
    ; closest_point : Gg.v2
    ; ratio : float
    ; p0 : Point.t
    ; p1 : Point.t }

  (** Return the distance between a given point and the curve. May return
      None if the point is out of the curve *)
  val distance
    : Gg.v2 -> t -> approx option

  (** Iterate over a path *)
  val iter
    : t -> f:(Point.t -> unit) -> unit

  (** Map all the points in the path *)
  val map
    : t -> (Point.t -> Point.t) -> t

  (** Reevaluate all the control points on the path in order to get a smooth
      curve *)
  val rebuild 
    : t -> t option

  (** Delete a point in the path. 

      Reconnect the path without the point removed, and reevaluate all the
      control points from the nodes

      return None if the point is not present in the curve
  *)
  val remove_point 
    : t -> Point.t -> t option

  (** Replace a point by the given one. 

      An existing point with the same id shall be present in the path. 

      The path is not fully evaluated, and rebuild shall be runned in order to
      get the path completely smooth.

  *)
  val replace_point 
    : t -> Point.t -> t option

end
