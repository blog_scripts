module Path = Script_path

type 'a selection =
  | Path of 'a
  | Point of ('a * Path.Point.t)

type t = int selection

val threshold : float

val get_from_paths :
     float * float
  -> Outline.t list
  -> float * (Gg.v2 * Outline.t * Path.Point.t * Path.Point.t) option
(** Return the closest path from the list to a given point. 

    The path is returned with all thoses informations :
    - The point in the path 
    - The path itself
    - The starting point from the path
    - The end point in the path

*)

val select_path : Outline.t -> t

val select_point : Outline.t -> Gg.v2 -> t
(** Check for selecting a point on the given outline. 

    If no point is available, select the path.

*)

val find_selection :
  int selection -> Outline.t list -> Outline.t selection option
