module Path = Script_path

module Make (Repr : Repr.PRINTER) : sig
  type repr

  type t = Path.Point.t

  val create_path : 'b -> repr

  (* Start a new path. *)
  val start : Path.Point.t -> repr -> repr

  val line_to : Path.Point.t -> Path.Point.t -> repr -> repr

  val quadratic_to :
    Path.Point.t -> Gg.v2 -> Gg.v2 -> Path.Point.t -> repr -> repr

  val stop : repr -> repr

  val get : repr -> Repr.t
end
