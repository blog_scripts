module Path = Script_path

module Make (Layer : Repr.PRINTER) = struct
  type point = Path.Point.t

  let mark point path =
    let open Gg.V2 in
    let point = Path.Point.get_coord point in

    let dist = 5.
    and dist' = -5. in

    let path =
      Layer.move_to (point - of_tuple (dist, dist)) path
      |> Layer.line_to (point + of_tuple (dist, dist))
      |> Layer.move_to (point + of_tuple (dist', dist))
      |> Layer.line_to (point + of_tuple (dist, dist'))
    in
    path


  type t = { path : Layer.t }

  type repr = Layer.t

  let create_path : 'b -> t = fun _ -> { path = Layer.create () }

  let start : point -> point -> t -> t =
   fun p1 _ { path } ->
    let path = mark p1 path in
    { path }


  let line_to : point * point -> point * point -> t -> t =
   fun (p0, p1) _ { path } ->
    let path =
      Layer.move_to (Path.Point.get_coord p0) path
      |> Layer.line_to (Path.Point.get_coord p1)
      |> mark p1
    in
    { path }


  let quadratic_to :
      point * Gg.v2 * Gg.v2 * point -> point * Gg.v2 * Gg.v2 * point -> t -> t =
   fun (p0, ctrl0, ctrl1, p1) _ { path } ->
    let path =
      Layer.move_to (Path.Point.get_coord p0) path
      |> Layer.quadratic_to ctrl0 ctrl1 (Path.Point.get_coord p1)
      |> mark p1
    in

    { path }


  let stop : t -> t = fun path -> path

  let get : t -> Layer.t = fun { path; _ } -> path
end
