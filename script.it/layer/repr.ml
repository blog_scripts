module Path = Script_path

module type PRINTER = sig
  type t

  val create : unit -> t

  (* Start a new path. *)
  val move_to : Gg.v2 -> t -> t

  val line_to : Gg.v2 -> t -> t

  val quadratic_to : Gg.v2 -> Gg.v2 -> Gg.v2 -> t -> t
  (** [quadratic_to ctrl0 ctrl1 p1] create a quadratic curve from the current
      point to [p1], with control points [ctrl0] and [ctrl1] *)

  val close : t -> t
  (** Request for the path to be closed *)
end

module type ENGINE = sig
  type t

  type point = Path.Point.t

  type repr

  val get : t -> repr

  val create_path : (repr -> repr) -> t

  val start : point -> point -> t -> t

  val line_to : point * point -> point * point -> t -> t

  val quadratic_to :
    point * Gg.v2 * Gg.v2 * point -> point * Gg.v2 * Gg.v2 * point -> t -> t

  val stop : t -> t
end
