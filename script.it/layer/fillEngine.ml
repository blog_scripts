module Path = Script_path

module Make (Layer : Repr.PRINTER) = struct
  type point = Path.Point.t

  type repr = Layer.t

  type t =
    { path : Layer.t
    ; close : Layer.t -> Layer.t
    }

  let create_path : (Layer.t -> Layer.t) -> t =
   fun f -> { close = f; path = Layer.create () }


  (* Start a new path. *)

  let start : point -> point -> t -> t =
   fun p1 _ { close; path } ->
    let path = Layer.move_to (Path.Point.get_coord p1) path in
    { close; path }


  let line_to : point * point -> point * point -> t -> t =
   fun (p0, p1) (p0', p1') t ->
    let p0 = Path.Point.get_coord p0
    and p1 = Path.Point.get_coord p1
    and p0' = Path.Point.get_coord p0'
    and p1' = Path.Point.get_coord p1' in

    let path =
      Layer.move_to p1 t.path
      |> Layer.line_to p1'
      |> Layer.line_to p0'
      |> Layer.line_to p0
      |> Layer.line_to p1
      |> Layer.close
    in
    let path = t.close path in
    { t with path }


  let quadratic_to :
      point * Gg.v2 * Gg.v2 * point -> point * Gg.v2 * Gg.v2 * point -> t -> t =
   fun (p0, ctrl0, ctrl1, p1) (p0', ctrl0', ctrl1', p1') t ->
    let p0 = Path.Point.get_coord p0
    and p1 = Path.Point.get_coord p1
    and p0' = Path.Point.get_coord p0'
    and p1' = Path.Point.get_coord p1' in

    let path =
      Layer.move_to p1 t.path
      |> Layer.line_to p1'
      (* Backward *)
      |> Layer.quadratic_to ctrl1' ctrl0' p0'
      |> Layer.line_to p0
      (* Forward *)
      |> Layer.quadratic_to ctrl0 ctrl1 p1
      |> Layer.close
      |> t.close
    in

    { t with path }


  let stop : t -> t = fun t -> t

  let get : t -> Layer.t = fun t -> t.path
end
