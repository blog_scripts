module Path = Brr_canvas.C2d.Path
module V2 = Gg.V2

type t = Path.t

let create
  : unit -> t
  = Path.create

(* Start a new path. *)
let move_to
  : Gg.v2 -> t -> t
  = fun point path ->
    let x, y = V2.to_tuple point in
    Path.move_to ~x ~y path;
    path

let line_to
  : Gg.v2 -> t -> t
  = fun  point path ->
    let x, y = V2.to_tuple point in
    Path.line_to ~x ~y path;
    path

let quadratic_to
  : Gg.v2 -> Gg.v2 -> Gg.v2 -> t -> t
  = fun  ctrl0 ctrl1 p1 path ->
    let cx, cy = V2.to_tuple ctrl0
    and cx', cy' = V2.to_tuple ctrl1
    and x, y = V2.to_tuple p1 in
    Path.ccurve_to
      ~cx ~cy
      ~cx' ~cy'
      ~x ~y
      path;
    path

let close
  : t -> t
  = fun path ->
    Path.close path;
    path
