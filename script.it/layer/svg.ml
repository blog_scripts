(** SVG representation *)

open Brr

module V2 = Gg.V2

let svg : El.cons
  = fun ?d ?at childs ->
    El.v ?d ?at (Jstr.v "svg") childs

let path: El.cons
  = fun ?d ?at childs ->
    El.v ?d ?at (Jstr.v "path") childs

type t = Jstr.t

let create
  : unit -> t
  = fun () -> Jstr.empty

(* Start a new path. *)
let move_to
  : Gg.v2 -> t -> t
  = fun point path ->
    let x, y = V2.to_tuple point in

    Jstr.concat ~sep:(Jstr.v " ")
      [ path
      ; Jstr.v "M"
      ; Jstr.of_float x
      ; Jstr.of_float y ]

let line_to
  : Gg.v2 -> t -> t
  = fun  point path ->
    let x, y = V2.to_tuple point in
    Jstr.concat ~sep:(Jstr.v " ")
      [ path
      ; (Jstr.v "L")
      ; (Jstr.of_float x)
      ; (Jstr.of_float y) ]

let quadratic_to
  : Gg.v2 -> Gg.v2 -> Gg.v2 -> t -> t
  = fun  ctrl0 ctrl1 p1 path ->
    let cx, cy = V2.to_tuple ctrl0
    and cx', cy' = V2.to_tuple ctrl1
    and x, y = V2.to_tuple p1 in
    Jstr.concat ~sep:(Jstr.v " ")
      [ path
      ; (Jstr.v "C")
      ; (Jstr.of_float cx)
      ; (Jstr.of_float cy)
      ; (Jstr.v ",")
      ; (Jstr.of_float cx')
      ; (Jstr.of_float cy')
      ; (Jstr.v ",")
      ; (Jstr.of_float x)
      ; (Jstr.of_float y) ]

let close
  : t -> t
  = fun path ->
    Jstr.append path (Jstr.v " Z")
