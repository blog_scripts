open Js_of_ocaml
module Path = Script_path

let ( let=? ) : 'a option -> ('a -> unit) -> unit =
 fun f opt -> Option.iter opt f


let post_message : Worker_messages.from_worker -> unit = Worker.post_message

let rebuild outline =
  let path = outline.Outline.path in

  let=? path = Path.Fixed.rebuild path in
  let back =
    Path.Fixed.map path (fun pt ->
        Path.Point.copy pt @@ Path.Point.get_coord' pt )
  in
  let=? back = Path.Fixed.rebuild back in
  post_message (`Complete { outline with path; back })


let execute (command : Worker_messages.to_worker) =
  match command with
  (* Full rebuild, evaluate the whole path *)
  | `Complete outline -> rebuild outline
  (* Remove the point from the main line, and reevaluate the whole path *)
  | `DeletePoint (point, outline) ->
      let=? path = Path.Fixed.remove_point outline.Outline.path point in
      rebuild { outline with path }
  (* Only evaluate the interior *)
  | `Back outline ->
      let back =
        Path.Fixed.map outline.Outline.path (fun pt ->
            Path.Point.copy pt @@ Path.Point.get_coord' pt )
      in
      let=? back = Path.Fixed.rebuild back in
      post_message (`Complete { outline with back })
  | `TranslatePath (outline, delta) ->
      let path =
        Path.Fixed.map outline.path (fun pt ->
            Path.Point.get_coord pt |> Gg.V2.add delta |> Path.Point.copy pt )
      and back =
        Path.Fixed.map outline.back (fun pt ->
            Path.Point.get_coord pt |> Gg.V2.add delta |> Path.Point.copy pt )
      in
      post_message (`Complete { outline with path; back })
  | `TranslatePoint (point, outline) ->
      (* I do not use the function Path.Fixed.replace_point here, I just
         replace the point position and run a full rebuild *)
      let path =
        Path.Fixed.map outline.path (fun pt ->
            match Path.Point.id pt = Path.Point.id point with
            | true -> point
            | false -> pt )
      in

      rebuild { outline with path }


let () = Worker.set_onmessage execute
