open Js_of_ocaml
module Path = Script_path

type to_worker =
  [ `Complete of Outline.t
  | `DeletePoint of Path.Point.t * Outline.t
  | (* Update the interior path *)
    `Back of Outline.t
  | (* Translate a path *)
    `TranslatePath of Outline.t * Gg.v2
  | `TranslatePoint of Path.Point.t * Outline.t
  ]

type from_worker =
  [ `Complete of Outline.t
  | `Other of Js.js_string Js.t
  ]
