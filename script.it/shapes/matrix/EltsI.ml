module type ORDERED_AND_OPERATIONAL =
sig

  (* Exception for from_string. Is raised when from_string is passed something
   * that is not an elt *)
  exception NonElt

  type t

  (* The zero element *)
  val zero : t

  (* The one element *)
  val one: t

  (* ts must be comparable *)
  val compare : t -> t -> Order.order

  (* Basic mathematical operations must be possible *)
  val add: t -> t -> t

  val subtract: t -> t -> t

  val multiply: t -> t -> t

  val divide: t -> t -> t

end
