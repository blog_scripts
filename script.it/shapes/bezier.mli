type t =
  { p0:Gg.v2      (* The starting point *)
  ; p1:Gg.v2      (* The end point *)
  ; ctrl0:Gg.v2   (* The control point *)
  ; ctrl1:Gg.v2 } (* The control point *)

type quadratic

(**
   Build a control point for a quadratic curve for passing throuht 3 points.
   taken from https://xuhehuan.com/2608.html


   also look to https://pomax.github.io/bezierinfo/#pointcurves
*)
val three_points_quadratic
  : Gg.v2 -> Gg.v2 -> Gg.v2 -> quadratic

(**
   Create a curve from three points.

   This is an implementation for
   https://pomax.github.io/bezierinfo/#pointcurves

*)
val three_points_cubic
  : float -> Gg.v2 -> Gg.v2 -> Gg.v2 -> t

val quadratic_to_cubic
  : quadratic -> t

(** Split a bezier curve in two at a given position *)
val slice
  : float -> t -> t * t

(** Return the closest point to the curve by approximation *)
val get_closest_point
  : Gg.v2 -> t -> float * Gg.v2

val reverse: t -> t
