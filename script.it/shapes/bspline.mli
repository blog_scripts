type t

type err = 
  [ `InvalidPath (* Too few points in the path for building the curve *)
  ]

(** Convert a list of points into a beziers curves. 

    At least 4 points are required for building the path.

    [to_bezier ~connexion points] create a list of beziers segments joining all
    the points together. 

    [connexion0] add a virtual point in the begining for helping to get the
    appropriate tangent when connecting path together

    [connexion1] does the same at the end

*)
val to_bezier
  :  ?connexion0:Gg.v2
  -> ?connexion1:Gg.v2
  -> Gg.v2 list 
  -> (Bezier.t array, [> err]) Result.t
