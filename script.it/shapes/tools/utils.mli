(** Return a normalize angle *)
val norm_angle 
  : Gg.v2 -> float

(** return the interesction for two segments *)
val intersection
  : (Gg.v2 * Gg.v2) -> (Gg.v2 * Gg.v2) -> Gg.v2 option

(** Return the center of the cercle for three points  
    None if the point cannot be evaluated
*)
val center
  : Gg.v2 -> Gg.v2 -> Gg.v2 -> Gg.v2 option

(** Rotate the vector by the given angle *)
val rotate
  : Gg.v2 -> float -> Gg.v2

(** Test equality between two points *)
val equal_point
  : float -> Gg.v2 -> Gg.v2 -> bool
