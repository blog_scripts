module State = Script_state.State
module Selection = Script_state.Selection
module Path = Script_path

let update_property worker state value f = function
  | None -> state
  | Some (Selection.Path outline) ->
      (* Change width for the whole path *)
      let outline =
        { outline with
          Outline.path =
            Path.Fixed.map outline.Outline.path (fun p -> f p value)
        }
      in
      State.post worker (`Back outline);
      state
  | Some (Point (outline, point)) ->
      let path =
        Path.Fixed.map outline.path (fun pt ->
            match Path.Point.id pt = Path.Point.id point with
            | false -> pt
            | true -> f pt value )
      in
      let outline = { outline with path } in
      State.post worker (`Back outline);
      state


type t =
  { prop : [ `Angle | `Width ]
  ; value : float
  ; worker : Brr_webworkers.Worker.t
  }

let process { prop; value; worker } state =
  match prop with
  | `Angle ->
      let angle = value in
      ( match state.State.mode with
      | Selection t ->
          let state = { state with angle } in
          Selection.find_selection t state.paths
          |> update_property worker state angle Path.Point.set_angle
      | _ -> { state with angle } )
  | `Width ->
      let width = value in
      ( match state.State.mode with
      | Selection t ->
          let state = { state with width } in
          Selection.find_selection t state.paths
          |> update_property worker state width Path.Point.set_width
      | _ -> { state with width } )
