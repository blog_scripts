open StdLabels
module State = Script_state.State

type t = Outline.t

let process newPath state =
  let paths = List.map
      state.State.paths
      ~f:(fun line ->
          match Outline.(newPath.id = line.id) with
          | true -> newPath
          | false -> line) in
  { state with paths }

