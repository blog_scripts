module State = Script_state.State

type t = float * (float * float)

(** Tick event

    Tick only occurs when drawing a new path

*)
let process (delay, point) state =
  match state.State.mode with
  | Edit ->
    (* Add the point in the list *)
    let current = State.insert_or_replace
        state
        point
        delay
        state.current in
    { state with current }
  | _ -> state
