open StdLabels
open Brr
module State = Script_state.State
module Path = Script_path

type t = unit

let process () state =
  let my_host = Uri.host @@ Window.location @@ G.window in
  ( if Hashtbl.hash my_host = Blog.Hash_host.expected_host
  then
    (* Convert the path into an sVG element *)
    let svg =
      Layer.Svg.svg
        ~at:
          Brr.At.
            [ v (Jstr.v "xmlns") (Jstr.v "http://www.w3.org/2000/svg")
            ; v (Jstr.v "xmlns:xlink") (Jstr.v "http://www.w3.org/1999/xlink")
            ]
        (List.map state.State.paths ~f:(fun path ->
             Layer.Paths.to_svg
               ~color:Blog.Nord.nord0
               (module Path.Fixed)
               Outline.(path.path, path.back)
               state.State.rendering ) )
    in
    let content = El.prop Elements.Prop.outerHTML svg in
    Elements.Transfert.send
      ~mime_type:(Jstr.v "image/svg+xml")
      ~filename:(Jstr.v "out.svg")
      content );
  state
