(** Delete the selected element *)

open StdLabels
module State = Script_state.State
module Selection = Script_state.Selection

type t = { worker : Brr_webworkers.Worker.t }

(* Click anywhere while in Out mode, we switch in edition *)
let process { worker } state =
  match state.State.mode with
  | Selection (Path id) ->
    let paths = List.filter
        state.State.paths
        ~f:(fun p ->
            p.Outline.id != id
          ) in
    { state with paths ; mode = Out}

  | Selection (Point (id, point)) ->
    List.iter
      state.State.paths
      ~f:(fun p ->
          let id' = p.Outline.id in
          match id' = id with
          | false -> ()
          | true -> State.post worker (`DeletePoint (point, p))
        );
    { state with mode = Selection (Path id) }

  | _ -> state
