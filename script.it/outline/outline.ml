open StdLabels
module Path = Script_path

let internal_path_id = ref 0

type t =
  { id : int
  ; path : Path.Fixed.t
  ; back : Path.Fixed.t
  }

let get_id () =
  let id = !internal_path_id in
  incr internal_path_id;
  id


let find : t list -> int -> t option =
 fun ts id -> List.find_opt ts ~f:(fun p -> p.id = id)
