open Ppxlib

(**

   This is a simple ppx which evaluate hash for string at compilation time.

    [%static_hash "deadbeef"] is equivalent to [Hashtbl.hash "deadbeef"]

    the ppx only evaluate strings.
*)

let name = "static_hash"

let expand ~loc ~path:_ (value : string) =
  let h = Hashtbl.hash value in
  Ast_builder.Default.eint ~loc h

let extension =
  Extension.declare
    name
    Extension.Context.expression
    Ast_pattern.(single_expr_payload (estring __))
    expand



let rule = Ppxlib.Context_free.Rule.extension extension

let () =
  Driver.register_transformation
    ~rules:[rule]
    name
