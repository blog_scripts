open Js_of_ocaml

(** Compare two arrays *)
let compare
  : f:('a -> 'a -> bool Js.t) -> 'a Js.js_array Js.t -> 'a Js.js_array Js.t -> bool Js.t
  = fun ~f arr1 arr2 ->
    if arr1##.length <> arr2##.length then
      Js._false
    else
      arr1##every
        (Js.wrap_callback @@ fun elem i _ ->
         Js.Optdef.case (Js.array_get arr2 i)
           (fun () -> Js._false)
           (fun elem2 -> f elem elem2))
