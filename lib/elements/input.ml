open Brr
open Note
open Note_brr

(** Create a slider element, and a signal with the value. 

 [at] is the attribute list given to the element *)
let slider : at:Brr.At.t list -> Brr.El.t * float S.t =
 fun ~at ->
  let slider = El.input ~at () in

  let init_value = Jstr.to_float (El.prop El.Prop.value slider) in

  let event =
    Evr.on_el Ev.input
      (fun _ ->
        let raw_value = El.prop El.Prop.value slider in
        Jstr.to_float raw_value)
      slider
    |> S.hold init_value
  in
  (slider, event)

type file = {
    file : File.t
  ; content : Jstr.t
}

(** Read the content from the file *)
let file_loader : file Note.E.send -> File.t -> unit =
 fun event file ->
  let blob = File.as_blob file in
  Fut.await (Blob.text blob)
    (Result.iter (fun content -> event { file; content }))

(** Create an imput which load a file.

    [file_loader (Jstr.v ".json"] will create an input which only accept json
    files, and an event which gives access to the file.

*)
let file_loader : Jstr.t -> Brr.El.t * file Note.event =
 fun selector ->
  let add_file_event, add_file_sender = Note.E.create () in

  let i =
    El.input ()
      ~at:[ At.type' (Jstr.v "file"); (At.v (Jstr.v "accept")) selector ]
  in

  (* The event return a list of files.

     We are only interested by a single on, and keep only the first from the
     list. *)
  let on_change files = file_loader add_file_sender (List.hd files) in

  let _ =
    Ev.listen Ev.change
      (fun _e -> on_change (El.Input.files i))
      (El.as_target i)
  in

  (i, add_file_event)
