open Note_brr_kit

type t

val create : unit -> t * Time.span Note.E.t
val start : t -> float -> unit
val stop : t -> unit
val delay : t -> float
