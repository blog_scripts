open Note_brr_kit

type t = {
    mutable id : Brr.G.timer_id
  ; send : float Note.E.send
  ; mutable counter : Time.counter
}

let create : unit -> t * Note_brr_kit.Time.span Note.E.t =
 fun () ->
  let event, send = Note.E.create () and counter = Time.counter () in
  ({ id = -1; send; counter }, event)

let stop : t -> unit = fun { id; _ } -> Brr.G.stop_timer id

let start : t -> float -> unit =
 fun t d ->
  let { id; send; _ } = t in
  t.counter <- Time.counter ();

  Brr.G.stop_timer id;
  let timer_id =
    Brr.G.set_interval
      ~ms:(int_of_float @@ (d *. 1000.))
      (fun () ->
        let span = Time.counter_value t.counter in
        send span)
  in
  t.id <- timer_id

let delay : t -> float = fun t -> Time.counter_value t.counter
