open Brr

include El.Prop

let offsetWidth
  : int t
  = El.Prop.int (Jstr.v "offsetWidth")

let offsetHeight
  : int t
  = El.Prop.int (Jstr.v "offsetHeight")

let outerHTML
  : Jstr.t t
  = El.Prop.jstr (Jstr.v "outerHTML")


let value
  : Jstr.t t
  = El.Prop.jstr (Jstr.v "value")
