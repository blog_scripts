open StdLabels
open Brr

(** Return the sidebar *)
let get
  : unit -> El.t option
  = fun () ->

    List.find_opt (El.children @@ Document.body G.document)
      ~f:(fun t -> El.has_tag_name El.Name.aside t)

let rec clean
  : El.t -> unit
  = fun el ->
    List.iter (El.children el)
      ~f:(fun el ->
          (* Remove the links from the sidebar, keep h1 and other stuff *)
          if (El.has_tag_name (Jstr.v "nav") el)
          || (El.has_tag_name (Jstr.v "ul") el) then
            El.remove el
          else
            clean el
        )
