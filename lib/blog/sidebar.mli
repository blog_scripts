(** Return the sidebarr element *)
val get
  : unit -> Brr.El.t option

(** Clean the sidebarr from anay element *)
val clean
  : Brr.El.t -> unit
