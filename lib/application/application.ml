module Make (S : sig
  type t
end) =
struct
  module type Procesor = sig
    type t

    val process : t -> S.t -> S.t
  end

  type event = E : 'a * (module Procesor with type t = 'a) -> event

  (** Simple helper for the main event loop *)
  let run : ?eq:(S.t -> S.t -> bool) -> S.t -> event Note.E.t -> S.t Note.S.t =
   fun ?eq init event ->
    let action =
      Note.E.map (fun (E (t, (module P))) st -> P.process t st) event
    in
    Note.S.accum ?eq init action


  let dispatch : (module Procesor with type t = 's) -> 's -> event =
    fun (type s) (module P : Procesor with type t = s) v -> E (v, (module P))
end
