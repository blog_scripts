type t

val empty
  : t

val add_css
  : t -> Css.Types.Stylesheet.t -> t

val extract_css
  : t -> Css.Types.Stylesheet.t
