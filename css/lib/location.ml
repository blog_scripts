type t = {

  loc_start : Lexing.position;
  loc_end : Lexing.position;
  loc_ghost : bool;
}

let none =
  { loc_start = Lexing.dummy_pos
  ; loc_end = Lexing.dummy_pos
  ; loc_ghost = true
  }

let  register_error_of_exn _ = ()

let error ~loc ?(sub=[]) _ =
  let _ = loc
  and _ = sub in
  ()
