#!/bin/sh
# Split the given dictionnary in files containing words of given length (1 to 10)
mkdir -p dicts
for i in $(seq 1 10); do
    LANG=C grep -E "^.{$i}\$" $1 > dicts/$1_$i
done
