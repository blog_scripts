type t

val analyse : Wordlist.t -> t
(** Evaluate the score for each char (lower is better) *)

val pick_next_word : Wordlist.t -> t -> string
(** Get the word which with the most information in it.

The information is the score given to each character, representing each
frequency in the whole word list (lower is better). If the same letter is
present many times, we consider that succeding letters does not give any more
informations (do not consider the position here) *)
