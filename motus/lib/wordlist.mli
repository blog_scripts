(** The dictionnary *)

type t

val empty_data : unit -> t
(** Create an empty dictionnary. The resulting dictionnay cannot be used, it is
    juste here if required for initialisation *)

val words : t -> string Seq.t
(** Load all the words *)

val pick : t -> string option

val list_size : t -> int
(** Number of words in the list *)

val add_words : Criteria.t list -> string Seq.t -> t
(** Create a word list from an initial sequence. 

    Checks are made against the differents criteria in order to ensure that the
    word is valid *)

val filter : Criteria.t list -> t -> t
(** Remove all the words that does not match the criterias. *)

val remove_word : t -> string -> t
(** Remove a word from this list. This function is called when a proposition
    from the application is not recognized by the game. *)

val mem : string -> t -> bool
