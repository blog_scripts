type t =
  | Wellplaced
  | Misplaced
  | Missing

val elements : float
(** Number of elements in the sum type *)

val sequence : int -> t array Seq.t
(** Build a sequence of all the possible status for a given number of letters *)

val index_of_result : t array -> int
(** Get the index of a validity result *)

val index_to_result : base:int -> int -> t array

val to_criteria : char -> int -> t -> Criteria.t list -> Criteria.t list

val to_criterias : string -> t array -> Criteria.t list
(** Convert the validity result into a Criteria list, in the context of a given
    word  *)
