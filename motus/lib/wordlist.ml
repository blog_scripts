open StdLabels
module S = Set.Make (String)

type t = S.t

let empty_data () = S.empty

let add_words : Criteria.t list -> string Seq.t -> t =
 fun filters words ->
  Seq.filter
    (fun word -> List.for_all ~f:(Criteria.check_filter word) filters)
    words
  |> S.of_seq


let filter : Criteria.t list -> t -> t =
 fun filters t ->
  S.filter (fun word -> List.for_all ~f:(Criteria.check_filter word) filters) t


let words = S.to_seq

let list_size = S.cardinal

let remove_word t w = S.remove w t

let pick = S.choose_opt

let mem = S.mem
