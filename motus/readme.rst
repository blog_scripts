.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Motus est une application permettant de résoudre les jeux de type "motus", dans
lesquels il faut trouver un mot dans un dictionnaire.

Il est attendu des informations de type : la lettre est mal placée, la lettre
est absente afin que que le jeu puisse trouver le mot.

