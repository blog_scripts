open StdLabels
open Motus_lib

type proposition = (Jstr.t * Validity.t) option list

type state =
  { analysis : Wordlist.t
  ; catalog : Wordlist.t
  ; rules : Criteria.t list
  ; length : int
  ; propositions : proposition list
  ; current_prop : proposition
  ; fields : Brr.El.t list
  }

let init () =
  { analysis = Wordlist.empty_data ()
  ; catalog = Wordlist.empty_data ()
  ; rules = []
  ; length = 0
  ; propositions = []
  ; current_prop = []
  ; fields = []
  }


module App = Application.Make (struct
  type t = state
end)

(** Get the current rules to apply with from the field list *)
let get_current_rules : proposition -> Criteria.t list =
 fun prop ->
  let rules = ref [] in
  List.iteri prop ~f:(fun i prop ->
      Option.iter
        (fun (letter, validity) ->
          if Jstr.equal Jstr.empty letter
          then ()
          else
            let char = String.get (Jstr.to_string letter) 0 in
            rules := Validity.to_criteria char i validity !rules )
        prop );
  List.rev !rules


(** Compare two states *)
let eq : state -> state -> bool =
 fun s1 s2 ->
  (s1.length, s1.rules, s1.current_prop, s1.propositions, s1.analysis)
  = (s2.length, s2.rules, s2.current_prop, s2.propositions, s2.analysis)
