(** Reload the list without the current proposition *)

open StdLabels

type t = unit

let process : t -> State.state -> State.state =
 fun () state ->
  (* Get the word corresponding to the proposition *)
  let word =
    state.State.current_prop
    |> List.to_seq
    |> Seq.map (fun opt ->
           match opt with
           | None -> ' '
           | Some (letter, _) -> String.get (Jstr.to_string letter) 0 )
    |> String.of_seq
  in

  let new_state =
    { state with
      analysis = Motus_lib.Wordlist.remove_word state.analysis word
    ; catalog = Motus_lib.Wordlist.remove_word state.catalog word
    }
  in
  (* Get the new proposition if any *)
  let current_prop =
    Initialize.get_proposition
      ~catalog:new_state.catalog
      state.length
      new_state.analysis
      state.rules
  in
  match current_prop with
  | None -> new_state
  | Some prop ->
      FieldList.set_with_props prop new_state.fields new_state.rules;
      State.{ new_state with current_prop = prop }
