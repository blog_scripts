(** Load the next proposition from the word list *)

open StdLabels

type t = unit

let process : t -> State.state -> State.state =
 fun () state ->
  (* Get the news rules (and match the words only against the new ones )*)
  let rules = State.get_current_rules state.current_prop in

  (* Update the word list with the new rules *)
  let analysis = Motus_lib.Wordlist.filter rules state.analysis in
  Printf.printf
    "Number of elements after filter : %d\n"
    (Motus_lib.Wordlist.list_size analysis);

  let rules =
    Motus_lib.Criteria.merge_lists ~init:state.rules rules
    |> List.sort_uniq ~cmp:Stdlib.compare
  in

  let propositions = state.current_prop :: state.propositions
  and current_prop = [] in

  let new_state = { state with propositions; current_prop; rules; analysis } in

  (* Get the new proposition if any *)
  let current_prop =
    Initialize.get_proposition
      ~catalog:state.catalog
      state.length
      analysis
      rules
  in
  match current_prop with
  | None -> new_state
  | Some prop ->
      FieldList.set_with_props prop state.fields new_state.rules;
      State.{ new_state with current_prop = prop }
