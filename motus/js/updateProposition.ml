(** Update the current propositions when the user change a value in one of the
    field.
 *)

open StdLabels

type t =
  { position : int
  ; letter : Jstr.t
  ; validity : Motus_lib.Validity.t
  }

let process { position; letter; validity } state =
  let current_prop =
    List.mapi state.State.current_prop ~f:(fun pos' content ->
        if position <> pos'
        then content
        else if Jstr.is_empty letter
        then None
        else Some (Jstr.uppercased letter, validity) )
  in
  FieldList.set_with_props current_prop state.fields state.rules;

  { state with current_prop }
