open StdLabels
open OUnit2
open Motus_lib

let format_validity = function
  | Validity.Wellplaced -> 'W'
  | Validity.Misplaced -> 'M'
  | Validity.Missing -> '_'


let printer : Validity.t array option -> string = function
  | None -> ""
  | Some v ->
      String.init (Array.length v) ~f:(fun i ->
          format_validity @@ Array.get v i )


let tests =
  "entropy test suite"
  >::: [ ( "Compare words 1"
         >:: fun _ ->
         let w = "Test" in

         let ref = (w, Entropy.CharSet.of_seq (String.to_seq w)) in

         assert_equal
           ~printer
           (Some Validity.[| Wellplaced; Wellplaced; Wellplaced; Wellplaced |])
           (Entropy.compare_words "Test" ~ref) )
       ; ( "Compare words 2"
         >:: fun _ ->
         let w = "ABC" in
         let ref = (w, Entropy.CharSet.of_seq (String.to_seq w)) in
         let result = Entropy.compare_words "DAC" ~ref in

         assert_equal
           ~printer
           (Some Validity.[| Missing; Misplaced; Wellplaced |])
           result )
       ]
