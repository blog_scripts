open StdLabels
open OUnit2
open Motus_lib

let validiy_tests =
  "validity test suite"
  >::: [ ( "Sequence of elements"
         >:: fun _ ->
         assert_equal
           243
           ( Validity.sequence 5
           |> List.of_seq
           |> List.sort_uniq ~cmp:Stdlib.compare
           |> List.length ) )
       ; ( "Index of element"
         >:: fun _ ->
         assert_equal 0 Validity.(index_of_result [| Wellplaced; Wellplaced |])
         )
       ; ( "Bijection for all the elements"
         >:: fun _ ->
         (* Create an array of 243 elements*)
         let arr = Array.make 243 false in
         let seq = Validity.sequence 5 in
         (* Populate the array *)
         Seq.iter
           (fun v ->
             let idx = Validity.index_of_result v in
             Array.set arr idx true )
           seq;

         (* Now count the elements set to true *)
         let count =
           Array.fold_left arr ~init:0 ~f:(fun acc value ->
               if value then acc + 1 else acc )
         in

         assert_equal 243 count )
       ]


let _ =
  run_test_tt_main ("main tests" >::: [ validiy_tests; Entropy_tests.tests ])
