
Ce dépot contient les différents scripts ou applications utilisés dans le
blog.

Script.it
=========

Cette application permet de dessiner en html comme si l’on utilisait une plume.

Motus
=====

Il s’agit du solveur du Sutom, basé sur un arbre de décision.


Viz.js
======

Couche à graphviz pour permettre la création de graphe de manière rapide.
