open Js_of_ocaml

let () =
  Worker.import_scripts
    [ "viz.js"
    ; "full.render.js"
    ]

let () = Worker.set_onmessage (fun content ->
    let (viz: Process.viz Js.t Js.constr)
      = Js.Unsafe.global##._Viz in

    let _ = Process.do_action (new%js viz) content in
    ()


  )

