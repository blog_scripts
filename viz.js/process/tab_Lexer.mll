{
  open Js_of_ocaml
  exception SyntaxError of string * Lexing.lexbuf
  exception Eof

  type arrow =
  | Forward
  | Backward
  | Both
  | None

  type jstring = Js.js_string Js.t

  type res = 
  | NewLine
  | Root of jstring
  | Entry of jstring * (jstring * jstring * arrow) option
  | Redirection of jstring * jstring * arrow
  | Separator

  let get_arrow = begin function
  | "<-" -> Backward
  | "<->" -> Both
  | "--" -> None
  | _ -> Forward
  end

}

let space = ['\000' '\t' '\x0C' ' '] 
let spaces = space*
let newline = spaces ("\r\n" | '\n' | '\r')+

(* Any character except the delimiters and spaces *)
let regular = [^ '\n' '\x0C' '\r' ]
let target_id = ['A'-'Z' 'a'-'z' '0'-'9']+ (':' ['0'-'9']+)?

let arrow = "->" | "--" | "<-" | "<->"

rule parse_line = shortest
  | eof            { raise Eof }

  | newline  {NewLine}

  | space+ '-'+ newline {Separator}

  | (target_id as _1) newline        { Root (Js.string _1) }

  | (spaces as _1) 
    (arrow as _arrow) space+ 
    (target_id as _target) spaces 
    (regular* as comment) newline 
        { Redirection ( Js.string _target, Js.string comment, (get_arrow _arrow))}

  | spaces
    (regular+ as _2) space+ 
    (arrow as _arrow) space+ 
    (target_id as _target) spaces 
    (regular* as comment) newline 
        { Entry (Js.string _2, Some (Js.string _target, Js.string comment, (get_arrow _arrow)))}

  | (spaces as _1) 
    (regular+ as _2) newline
        { Entry (Js.string _2, None) }

