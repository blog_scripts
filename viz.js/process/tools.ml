let drop_while predicate =
  let rec _drop = function
    | [] -> []
    | (hd::tl) as l ->
      if predicate hd then
        _drop tl
      else
        l
  in _drop

(* Split a text and and new line before it goes to long *)
let split limit =
  let rec _split elms text =
    let length = (String.length text) -1 in
    if (length < limit) then
      List.rev (text::elms)
      |> String.concat "\\n"
    else
      try
        let pos = String.rindex_from text limit ' ' in
        let hd = String.sub text 0 pos
        and tl = String.sub text (pos +1) (length - pos) in
        _split (hd::elms) tl
      with Not_found -> text
  in _split []

