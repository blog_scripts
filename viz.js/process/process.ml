open Js_of_ocaml

class type viz = object
  (* PNG output *)
  method renderImageElement: Js.js_string Js.t -> (Dom_html.imageElement Js.t, unit) Promise.promise Js.meth
  (* SVG output *)
  method renderSVGElement: Js.js_string Js.t -> (Dom_svg.svgElement Js.t, unit) Promise.promise Js.meth

  method renderString: Js.js_string Js.t -> (Js.js_string Js.t, unit) Promise.promise Js.meth
end

type t = Js.js_string Js.t * Js.js_string Js.t

let do_action
  : viz Js.t -> Js.js_string Js.t -> (unit, unit) Promise.promise
  = fun v text ->

    if text##.length != 0 then (
      let gv = Formatter.convert text in
      let promise =
        (try v##renderString gv
         with _ -> Promise.reject ()
        ) in
      Promise.then_map
        ~on_fulfilled:(fun svg -> Worker.post_message ((gv, svg):t))
        ~on_rejected:(fun _ -> ())
        promise
    ) else
      Promise.reject ()
