open Js_of_ocaml

type t

(** Register a new function with the given event *)
val register: 'a Dom.Event.typ -> ('a -> unit) -> t

(* Remove the handler for the event *)
val remove: 'a Dom.Event.typ -> t -> unit

(* Call the event *)
val call: 'a Dom.Event.typ -> 'a -> unit
