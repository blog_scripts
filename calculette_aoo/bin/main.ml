let cout_a = (800, 200, 100)
let cout_m = (110, 55, 35)
let cout_rm = (50, 40, 20)
let cout_pm = (5, 3, 1)
let cout_fm = (100, 50, 30)
let fm_oponent = 1
let n = 3
let ratio = Float.(to_int (pow 3.0 (of_int n)))

let print_arr arr =
  Format.printf "@[<v>%a@]@." (Format.pp_print_array Format.pp_print_int) arr

let () = print_arr (Aoo.Roll.build_frequencies n)

(* Calcule les probabilités de toucher contre les 10 prochains niveaux *)
let compare_result = Aoo.Roll.compare 9 1

let () =
  Format.printf "%.4f - %s\n"
    (compare_result |> Q.to_float)
    (compare_result |> Q.to_string)

let env : Aoo.Build.env =
  {
    cost_max = 1800
  ; max_tours = 5
  ; cout_sort = 8
  ; degat_sort = 6
  ; fm_oponent
  ; frequencies = Aoo.Build.buil_freq_table 9 fm_oponent
  }

(* Définition des caractéristiques *)
let a = Aoo.Carac.create 2 cout_a
let m = Aoo.Carac.create 5 cout_m
let rm = Aoo.Carac.create 7 cout_rm
let pm = Aoo.Carac.create 40 cout_pm
let fm = Aoo.Carac.create 9 cout_fm
let default = Aoo.Build.{ a; m; rm; pm; fm }
let score1 = Aoo.Build.score env default
let cost1 = Aoo.Build.cost default
let () = print_endline @@ Printf.sprintf "Score du build : %f" score1
let build3 = { default with fm = Aoo.Carac.incr ~step:1 fm }
let score3 = Aoo.Build.score env build3
let cost3 = Aoo.Build.cost build3
let () = print_endline @@ Printf.sprintf "Score du build : %f" score3
let () = print_endline @@ Printf.sprintf "%b" (score1 = score3)
let () = print_endline @@ Printf.sprintf "%b" (cost1 < cost3)

(*
   let () =
     let build =
       Aoo.Build.traverse env
         (Aoo.Build.cost default, Aoo.Build.score env default)
         (Aoo.Build.upgrade env default)
     in

     Aoo.Build.repr env Format.std_formatter build
*)
