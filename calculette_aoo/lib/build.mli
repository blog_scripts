type build = {
    a : Carac.t
  ; m : Carac.t
  ; rm : Carac.t
  ; pm : Carac.t
  ; fm : Carac.t
}

type env = {
    cout_sort : int
  ; degat_sort : int
  ; max_tours : int
  ; fm_oponent : int
  ; cost_max : int
  ; frequencies : (int * float) list
}

val buil_freq_table : int -> int -> (int * float) list
(** Build a list with the differents percentages to hit *)

val cost : build -> int
(** Get the cost for a build *)

val score : env -> build -> float
(** Get the score for the build *)

val traverse : env -> int * float -> build list -> build
(** Test differents upgrade configuration and present the best one with the
    constraints given in [env]. 

    The costt for the upgrade will not exceed the property [env.cost_max] 
    *)

val upgrade : env -> build -> build list
val repr : env -> Format.formatter -> build -> unit
