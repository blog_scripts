type cout_carac = int * int * int

type t = {
    value : int
  ; couts : cout_carac
  ; bonus : int
}

let create : ?bonus:int -> int -> cout_carac -> t =
 fun ?(bonus = 0) value couts -> { value; couts; bonus }

let incr ?(step = 1) t = { t with bonus = t.bonus + step }

(*
Evaluate the cost for the successives upgrades. 

I’m pretty sure this can be transformed into a linear function, but I do not see how…  

    c0 * t.bonus 
+   max 0 ((((t.bonus - 1) * 2) - 1) * c1)
+   ?

 *)
let cout : t -> int =
 fun t ->
  let c0, c1, c2 = t.couts in
  let rec c acc t =
    match t with
    | 0 -> acc
    | 1 -> acc + c0
    | 2 -> c (acc + c0 + c1) (t - 1)
    | 3 -> c (acc + c0 + (c1 * 2)) (t - 1)
    | n -> c (acc + c0 + (c1 * 2) + ((n - 3) * c2)) (t - 1)
  in
  c 0 t.bonus

let value t = t.value + t.bonus

let repr : Format.formatter -> t -> unit =
 fun out t -> Format.fprintf out "%d (+%d)" (value t) t.bonus
