type t
type cout_carac = int * int * int

val create : ?bonus:int -> int -> cout_carac -> t

val cout : t -> int
(** Get the cost for the upgrades for this property *)

val value : t -> int
(** Get the value (including upgrades) for this property *)

val incr : ?step:int -> t -> t
(** Increment this property. 
    step is default 1.
 *)

val repr : Format.formatter -> t -> unit
